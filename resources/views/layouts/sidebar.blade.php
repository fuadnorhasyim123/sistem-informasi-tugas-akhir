<aside class="left-sidebar bg-sidebar">
    <div id="sidebar" class="sidebar sidebar-with-footer">
        <!-- Aplication Brand -->
        <div class="app-brand">
            <a href="#">
                <svg class="brand-icon" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid" width="30" height="33" viewBox="0 0 30 33">
                    <g fill="none" fill-rule="evenodd">
                        <path class="logo-fill-blue" fill="#7DBCFF" d="M0 4v25l8 4V0zM22 4v25l8 4V0z" />
                        <path class="logo-fill-white" fill="#FFF" d="M11 4v25l8 4V0z" />
                    </g>
                </svg>
                <p class="brand-name" style="font-size:small; font-weight:bold">Sistem Informasi<br>Pendaftaran Dan Monitoring<br>Tugas Akhir</p>
            </a>
        </div>
        <!-- begin sidebar scrollbar -->
        <div class="sidebar-scrollbar">

            <!-- sidebar menu -->
            <ul class="nav sidebar-inner" id="sidebar-menu">
                @if(Auth::user()->level=='admin')
                <span class="mb-2 mr-2 badge badge-success" style="margin-left:5px;margin-top:-20px;text-align: center;color:white; font-size:large">ADMIN</span>
                <hr class="separator" />
                <li class="{{ Route::is('admin.dashboard') ? 'active' : '' }} expand">
                    <a class="sidenav-item-link" href="{{url('/admin/dashboard')}}">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        <span class="nav-text">Dashboard</span></b>
                    </a>
                </li>
                <li class="has-sub">
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#keloladata" aria-expanded="false" aria-controls="keloladata">
                        <i class="mdi mdi-chart-pie"></i>
                        <span class="nav-text">Kelola Data</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="keloladata" data-parent="#sidebar-menu">
                        <div class="sub-menu">
                            <li class="{{ Route::is('admin.mahasiswa') ? 'active' : '' }}">
                                <a class="sidenav-item-link" href="{{ url('/admin/mahasiswa') }}">
                                    <span class="nav-text">Mahasiswa</span>
                                </a>
                            </li>
                            <li class="{{ Route::is('admin.dosen') ? 'active' : '' }}">
                                <a class=" sidenav-item-link" href="{{ url('/admin/dosen') }}">
                                    <span class="nav-text">Dosen</span>
                                </a>
                            </li>
                            <li class="{{ Route::is('admin.koorta') ? 'active' : '' }}">
                                <a class=" sidenav-item-link" href="{{ url('/admin/koor-ta') }}">
                                    <span class="nav-text">Koordinator TA</span>
                                </a>
                            </li>
                        </div>
                    </ul>
                </li>
                @elseif(Auth::user()->level=='dosen')
                <span class="mb-2 mr-2 badge badge-success" style="margin-left:5px;margin-top:-20px;text-align: center;color:white; font-size:large">DOSEN</span>
                <hr class="separator" />
                <li class="has-sub  {{ Route::is('dosen.dashboard') ? 'active' : '' }} expand">
                    <a class="sidenav-item-link" href="{{url('/dosen/dashboard')}}">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        <span class="nav-text">Dashboard</span></b>
                    </a>
                </li>
                <li class="has-sub">
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#keloladata" aria-expanded="false" aria-controls="keloladata">
                        <i class="mdi mdi-chart-pie"></i>
                        <span class="nav-text">Pendaftaran TA</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="keloladata" data-parent="#sidebar-menu">
                        <div class="sub-menu">
                            <li class="{{ Route::is('dosen.persetujuan') ? 'active' : '' }}">
                                <a class="sidenav-item-link" href="/dosen/persetujuan/{{ Auth::user()->id }}">
                                    <span class="nav-text">Menunggu Persetujuan</span>
                                </a>
                            </li>
                        </div>
                        <div class="sub-menu">
                            <li class="{{ Route::is('dosen.pendaftaran') ? 'active' : '' }}">
                                <a class="sidenav-item-link" href="/dosen/pendaftaran/{{ Auth::user()->id }}">
                                    <span class="nav-text">Terdaftar</span>
                                </a>
                            </li>
                        </div>
                    </ul>
                </li>
                <li class="has-sub {{ Route::is('dosen.bimbingan') ? 'active' : '' }}">
                    <a class="sidenav-item-link" href="/dosen/bimbingan/{{ Auth::user()->id }}">
                        <i class="mdi mdi-chart-pie"></i>
                        <span class="nav-text">Bimbingan TA</span></b>
                    </a>
                </li>
                @elseif(Auth::user()->level=='koordinator')
                <span class="mb-2 mr-2 badge badge-success" style="margin-left:5px;margin-top:-20px;text-align: center;color:white; font-size:large">KOOR TA</span>
                <hr class="separator" />
                <li class="has-sub {{ Route::is('koor-ta.dashboard') ? 'active' : '' }} expand">
                    <a class="sidenav-item-link" href="{{url('/koor-ta/dashboard')}}">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        <span class="nav-text">Dashboard</span></b>
                    </a>
                </li>
                <li class="has-sub">
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#keloladata" aria-expanded="false" aria-controls="keloladata">
                        <i class="mdi mdi-chart-pie"></i>
                        <span class="nav-text">Kelola Data</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="keloladata" data-parent="#sidebar-menu">
                        <div class="sub-menu">
                            <li class="{{ Route::is('koor-ta.admin') ? 'active' : '' }}">
                                <a class="sidenav-item-link" href="{{ url('/koor-ta/admin') }}">
                                    <span class="nav-text">Admin</span>
                                </a>
                            </li>
                        </div>
                    </ul>
                </li>
                <li class="has-sub {{ Route::is('koor-ta.pendaftaran') ? 'active' : '' }}">
                    <a class="sidenav-item-link" href="{{url('/koor-ta/pendaftaran')}}">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        <span class="nav-text">Pendaftaran TA</span></b>
                    </a>
                </li>
                <li class="has-sub {{ Route::is('koor-ta.bimbingan') ? 'active' : '' }}">
                    <a class="sidenav-item-link" href="{{url('/koor-ta/bimbingan')}}">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        <span class="nav-text">Bimbingan TA</span></b>
                    </a>
                </li>
                @elseif(Auth::user()->level=='mahasiswa')
                <span class="mb-2 mr-2 badge badge-success" style="margin-left:5px;margin-top:-20px;text-align: center;color:white; font-size:large">MAHASISWA</span>
                <hr class="separator" />
                <li class="has-sub {{ Route::is('mahasiswa.dashboard') ? 'active' : '' }} expand">
                    <a class="sidenav-item-link" href="{{url('/mahasiswa/dashboard')}}">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        <span class="nav-text">Dashboard</span></b>
                    </a>
                </li>
                <li class="has-sub {{ Route::is('mahasiswa.pendaftaran') ? 'active' : '' }}">
                    <a class="sidenav-item-link" href="/mahasiswa/pendaftaran/{{ Auth::user()->id }}">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        <span class="nav-text">Pendaftaran TA</span></b>
                    </a>
                </li>
                <li class="has-sub {{ Route::is('mahasiswa.bimbingan') ? 'active' : '' }}">
                    <a class="sidenav-item-link" href="/mahasiswa/bimbingan/{{ Auth::user()->id }}">
                        <i class="mdi mdi-view-dashboard-outline"></i>
                        <span class="nav-text">Bimbingan TA</span></b>
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</aside>