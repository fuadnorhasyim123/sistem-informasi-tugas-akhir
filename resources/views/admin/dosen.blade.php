<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <div class="breadcrumb-wrapper">
                        <h1>Data Dosen</h1>

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/admin/dashboard') }}">
                                        <span class="mdi mdi-home"></span> Dashboard
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    Dosen
                                </li>
                            </ol>
                        </nav>
                    </div>
                    @include('sweetalert::alert')
                    <div class="col-l2">
                        <div class="card card-default">
                            <div class="card-body">
                                <form class="form">
                                    <div class="form-body">
                                        {{-- notifikasi form validasi --}}
                                        @if ($errors->has('file'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('file') }}</strong>
                                        </span>
                                        @endif
                                        <a href="javascript:void(0)" class="btn btn-primary mb-3" id="new-dosen" data-toggle="modal" style="margin-top: -10px;">
                                            <i class="mdi mdi-plus-box-outline"></i> Tambah
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-secondary mb-3" id="import-dosen" data-toggle="modal" style="margin-top: -10px;">
                                            <i class="mdi mdi-file-import"></i> Import
                                        </a>
                                        <a href="/document/Data Dosen.xlsx" class="btn btn-success mb-1" style="margin-top: -10px; float:right;">
                                            <i class="mdi mdi-plus-box-outline"></i> Download Draft Import
                                        </a>
                                        @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li><i class="mdi mdi-alert mr-1 mt-1 mb-3"></i>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        <div class="table-responsive">
                                            <table id="table_dosen" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">No.</th>
                                                        <th scope="col">NIDN</th>
                                                        <th scope="col">Nama</th>
                                                        <th scope="col">Alamat</th>
                                                        <th scope="col">No. Telp.</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach ($dosen as $data)
                                                    <tr>
                                                        <td style="text-align: center;">{{$no++}}</td>
                                                        <td>{{ $data->nidn }}</td>
                                                        <td>{{ $data->name }}</td>
                                                        <td>{{ $data->alamat }}</td>
                                                        <td>{{ $data->no_telp }}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" class="btn btn-warning" id="edit-dosen" data-toggle="modal" data-id="{{ $data->id }} "><i class="mdi mdi-square-edit-outline"></i></a>
                                                            <meta name="csrf-token" content="{{ csrf_token() }}">
                                                            <a href="dosen/hapus/{{$data->id}}" class="btn btn-danger delete-user" onclick="return confirm('Are you sure?')"><i class="mdi mdi-trash-can-outline"></i></a>
                                                        </td>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal fade" id="crud-modal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="dosencrudmodal"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form name="custForm" action="dosen/tambah" method="POST">
                                            <input type="hidden" name="dosenid" id="dosenid">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="nidn">NIDN</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-clipboard-account"></i>
                                                                </span>
                                                            </div>
                                                            <input type="number" name="dosen_nidn" id="dosen_nidn" class="form-control" onchange="validate()" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="nama">Nama</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-account"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="dosen_nama" id="dosen_nama" class="form-control" onchange="validate()" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="username">Username</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-security-account"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="dosen_username" id="dosen_username" class="form-control" onchange="validate()" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="no_telp">No. Telp</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-account-card-details"></i>
                                                                </span>
                                                            </div>
                                                            <input type="number" name="dosen_no_telp" id="dosen_no_telp" class="form-control" onchange="validate()" minlength="12" maxlength="12" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat</label>
                                                        <div class="input-group">
                                                            <textarea class="form-control" id="dosen_alamat" name="dosen_alamat" rows="4" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Submit</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalTooltip" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="doseneditmodal"></h5>
                                    </div>
                                    <div class="modal-body">
                                        <form name="custForm" action="dosen/edit/" method="POST">
                                            <input type="hidden" name="dosenid1" id="dosenid1">
                                            <input type="hidden" name="dosennidn1" id="dosennidn1">
                                            @csrf
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Reset</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="import-modal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="dosenimportmodal"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form name="custForm" action="dosen/import/" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="file">Pilih File</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-file"></i>
                                                                </span>
                                                            </div>
                                                            <input type="file" name="file" id="file" class="form-control" onchange="validate()" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Submit</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')
    @include('admin.modal-dosen')

</body>

</html>