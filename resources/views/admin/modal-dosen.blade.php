<script type="text/javascript">
    $(document).ready(function() {
        $('#table_dosen').DataTable();
    });
</script>

<script>
    $(document).ready(function() {

        /* When click Add status-perkawinan button */
        $('#new-dosen').click(function() {
            $('#btn-save').val("create-dosen");
            $('#dosen').trigger("reset");
            $('#dosencrudmodal').html("Tambah Dosen");
            $('#crud-modal').modal('show');
            $('#dosen_nidn').val("");
            $('#dosen_username').val("");
            $('#dosen_nama').val("");
            $('#dosen_alamat').val("");
            $('#dosen_no_telp').val("");
        });

        /* Edit status-perkawinan */
        $('body').on('click', '#edit-dosen', function() {
            var data_id = $(this).data('id');
            $.get('/admin/dosen/' + data_id + '/edit', function(data) {
                $('#doseneditmodal').html("Reset Password Dosen");
                $('#btn-update').val("Update");
                $('#btn-save').prop('disabled', false);
                $('#edit-modal').modal('show');
                $('#dosenid1').val(data.id);
                $('#dosennidn1').val(data.nidn);
            })
        });


        $('#import-dosen').click(function() {
            $('#btn-save').val("import-dosen");
            $('#dosen').trigger("reset");
            $('#dosenimportmodal').html("Import Data Dosen");
            $('#import-modal').modal('show');
        });
    });
</script>