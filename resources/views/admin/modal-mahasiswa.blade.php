<script type="text/javascript">
    $(document).ready(function() {
        $('#table_mahasiswa').DataTable();
    });
</script>

<script>
    $(document).ready(function() {

        $('#new-mahasiswa').click(function() {
            $('#btn-save').val("create-mahasiswa");
            $('#mahasiswa').trigger("reset");
            $('#mahasiswacrudmodal').html("Tambah Mahasiswa");
            $('#crud-modal').modal('show');
            $('#mhs_nim').val("");
            $('#mhs_nama').val("");
            $('#mhs_alamat').val("");
            $('#mhs_no_telp').val("");
            $('#mhs_username').val("");
        });

        $('body').on('click', '#edit-mahasiswa', function() {
            var data_id = $(this).data('id');
            $.get('/admin/mahasiswa/' + data_id + '/edit', function(data) {
                $('#mahasiswaeditmodal').html("Reset Password Mahasiswa");
                $('#btn-update').val("Update");
                $('#btn-save').prop('disabled', false);
                $('#edit-modal').modal('show');
                $('#mhsid1').val(data.id);
                $('#mhsnim1').val(data.nim);
            })
        });

        $('#import-mahasiswa').click(function() {
            $('#btn-save').val("import-mahasiswa");
            $('#mahasiswa').trigger("reset");
            $('#mahasiswaimportmodal').html("Import Data Mahasiswa");
            $('#import-modal').modal('show');
        });
    });
</script>