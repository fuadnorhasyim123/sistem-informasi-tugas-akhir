<script type="text/javascript">
    $(document).ready(function() {
        $('#table_koorta').DataTable();
    });
</script>

<script>
    $(document).ready(function() {

        /* When click Add status-perkawinan button */
        $('#new-koorta').click(function() {
            $('#btn-save').val("create-koorta");
            $('#koorta').trigger("reset");
            $('#koortacrudmodal').html("Tambah Koor TA");
            $('#crud-modal').modal('show');
            $('#koorta_nidn').val("");
            $('#koorta_nama').val("");
            $('#koorta_alamat').val("");
            $('#koorta_no_telp').val("");
            $('#koorta_username').val("");
        });

        /* Edit status-perkawinan */
        $('body').on('click', '#edit-koorta', function() {
            var data_id = $(this).data('id');
            $.get('/admin/koor-ta/' + data_id + '/edit', function(data) {
                $('#koortaeditmodal').html("Reset Password Koor TA");
                $('#btn-update').val("Update");
                $('#btn-save').prop('disabled', false);
                $('#edit-modal').modal('show');
                $('#koortaid1').val(data.id);
                $('#koortanidn1').val(data.nidn);
            })
        });


        $('#import-koorta').click(function() {
            $('#btn-save').val("import-koorta");
            $('#koorta').trigger("reset");
            $('#koortaimportmodal').html("Import Data Koor TA");
            $('#import-modal').modal('show');
        });
    });
</script>