<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <div class="breadcrumb-wrapper">
                        <h1>Data Pendaftaran TA</h1>

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/dosen/dashboard') }}">
                                        <span class="mdi mdi-home"></span> Dashboard
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    Pendaftaran TA
                                </li>
                            </ol>
                        </nav>

                    </div>
                    @include('sweetalert::alert')
                    <div class="col-l2">
                        <div class="card card-default">
                            <div class="card-body">
                                <form class="form">
                                    <div class="form-body">
                                        <div class="table-responsive">
                                            <table id="table_pendaftaran" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">No.</th>
                                                        <th scope="col">NIM</th>
                                                        <th scope="col">Nama Mahasiswa</th>
                                                        <th scope="col">Judul TA</th>
                                                        <th scope="col" width="60">Berkas 1</th>
                                                        <th scope="col" width="60">Berkas 2</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach ($pendaftaran as $data)
                                                    <tr>
                                                        <td style="text-align: center;">{{$no++}}</td>
                                                        <td>{{ $data->nim }}</td>
                                                        <td>{{ $data->nama_mhs }}</td>
                                                        <td>{{ $data->pendaftaran_judul }}</td>
                                                        @if($data->berkas1==null)
                                                        <td align="center"> -
                                                        </td>
                                                        @else
                                                        <td>
                                                            <a href="/document_mahasiswa/{{$data->berkas1}}">
                                                                <img src="{{ asset('assets/img/docx.png') }}" width="15" height="15"> Berkas 1
                                                            </a>
                                                        </td>
                                                        @endif
                                                        @if($data->berkas2==null)
                                                        <td align="center"> -
                                                        </td>
                                                        @else
                                                        <td>
                                                            <a href="/document_mahasiswa/{{$data->berkas2}}">
                                                                <img src="{{ asset('assets/img/docx.png') }}" width="15" height="15"> Berkas 2
                                                            </a>
                                                        </td>
                                                        @endif
                                                        <td>
                                                            <a href="javascript:void(0)" class="btn btn-warning" id="detail-pendaftaran" data-toggle="modal" data-id="{{ $data->nim }} "><i class="mdi mdi-information-outline"></i> Detail</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="detail-modal" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="detailmhsmodal"></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-striped" id="table_mhs">
                                                        <tbody>
                                                            <tr>
                                                                <th scope="col">NIM</th>
                                                                <td width="4">:</td>
                                                                <td scope="row" id="mhs_nim"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="col">Nama</th>
                                                                <td width="4">:</td>
                                                                <td id="mhs_nama"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="col">Alamat</th>
                                                                <td width="4">:</td>
                                                                <td id="mhs_alamat"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="col">No Telp</th>
                                                                <td width="4">:</td>
                                                                <td id="mhs_no_telp"></td>
                                                            </tr>
                                                            <tr id="berkas">
                                                                <th scope="col">Akun</th>
                                                                <td width="4">:</td>
                                                                <td>
                                                                    <i class="mdi mdi-circle-medium"></i> Username : <a id="mhs_username"></a></span><br>
                                                                    <i class="mdi mdi-circle-medium"></i> Email : <a id="mhs_email"></a></span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')
    @include('dosen.modal-pendaftaran')

</body>

</html>