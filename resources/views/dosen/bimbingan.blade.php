<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <div class="breadcrumb-wrapper">
                        <h1>Data Bimbingan TA</h1>

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/dosen/dashboard') }}">
                                        <span class="mdi mdi-home"></span> Dashboard
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    Bimbingan TA
                                </li>
                            </ol>
                        </nav>

                    </div>
                    @include('sweetalert::alert')
                    <div class="col-l2">
                        <div class="card card-default">
                            <div class="card-body">
                                <form class="form">
                                    <div class="form-body">
                                        <div class="table-responsive">
                                            <table id="table_bimbingan" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">No.</th>
                                                        <th scope="col">NIM</th>
                                                        <th scope="col">Nama Mahasiswa</th>
                                                        <th scope="col">Judul TA</th>
                                                        <th scope="col">Tanggal Bimbingan</th>
                                                        <th scope="col">Keterangan</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach ($bimbingan as $data)
                                                    <tr>
                                                        <td style="text-align: center;">{{$no++}}</td>
                                                        <td>{{ $data->nim }}</td>
                                                        <td>{{ $data->nama_mhs }}</td>
                                                        <td>{{ $data->pendaftaran_judul }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($data->bimbingan_tgl)->format('d-m-Y') }}</td>
                                                        <td>{{ $data->bimbingan_ket }}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" class="btn btn-primary mb-1" id="edit-ket" data-toggle="modal" data-id="{{ $data->bimbingan_id }}"><i class=" mdi mdi-comment"></i> Komentar</a>
                                                        </td>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal fade" id="edit-modal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="doseneditket"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form name="custForm" action="{{ Auth::user()->id }}/update/" method="POST">
                                            <input type="hidden" name="bimid" id="bimid">
                                            @csrf
                                            <div class="form-group">
                                                <textarea class="form-control" id="bimbingan_ket" name="bimbingan_ket" rows="6"></textarea>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Submit</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')
    @include('dosen.modal-bimbingan')

</body>

</html>