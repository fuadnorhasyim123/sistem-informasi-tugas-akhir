<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <div class="breadcrumb-wrapper">
                        <h1>Menunggu Persetujuan</h1>

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/dosen/dashboard') }}">
                                        <span class="mdi mdi-home"></span> Dashboard
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    Persetujuan TA
                                </li>
                            </ol>
                        </nav>

                    </div>
                    @include('sweetalert::alert')
                    <div class="col-l2">
                        <div class="card card-default">
                            <div class="card-body">
                                <form class="form">
                                    <div class="form-body">
                                        <div class="table-responsive">
                                            <table id="table_persetujuan" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">No.</th>
                                                        <th scope="col">NIM</th>
                                                        <th scope="col">Nama Mahasiswa</th>
                                                        <th scope="col">Judul TA</th>
                                                        <th scope="col">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach ($pendaftaran as $data)
                                                    <tr>
                                                        <td style="text-align: center;">{{$no++}}</td>
                                                        <td>{{ $data->nim }}</td>
                                                        <td>{{ $data->nama_mhs }}</td>
                                                        <td>{{ $data->pendaftaran_judul }}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" id="setuju" class="btn btn-primary" data-toggle="modal" data-id="{{ $data->pendaftaran_id }}"><i class="mdi mdi-file-check"></i> Setujui</a>
                                                            <a href="javascript:void(0)" id="tolak" class="btn btn-danger" data-toggle="modal" data-id="{{ $data->pendaftaran_id }}"><i class="mdi mdi-file-check"></i> Tolak</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalTooltip" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="setujutitle"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form name="custForm" action="{{ Auth::user()->id }}/update" method="POST">
                                            @csrf
                                            <input type="hidden" name="pendid" id="pendid" value="">
                                            <input type="hidden" name="action" id="action" value="">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Submit</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')
    @include('dosen.modal-persetujuan')

</body>

</html>