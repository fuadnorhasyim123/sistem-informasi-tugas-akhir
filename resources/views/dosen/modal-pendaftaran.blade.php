<script type="text/javascript">
    $(document).ready(function() {
        $('#table_pendaftaran').DataTable();
    });
</script>


<script>
    $(document).ready(function() {

        $('body').on('click', '#detail-pendaftaran', function() {
            var data_id = $(this).data('id');
            $.get('/dosen/pendaftaran/' + data_id + '/detail', function(data) {
                $('#detailmhsmodal').html("Detail Mahasiswa");
                $('#detail-modal').modal('show');
                $('#mhs_nim').html(data.nim);
                $('#mhs_nama').html(data.name);
                $('#mhs_alamat').html(data.alamat);
                $('#mhs_no_telp').html(data.no_telp);
                $('#mhs_email').html(data.email);
                $('#mhs_username').html(data.username);
            })
        });
    });
</script>