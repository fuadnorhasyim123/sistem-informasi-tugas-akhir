<script type="text/javascript">
    $(document).ready(function() {
        $('#table_persetujuan').DataTable();
    });
</script>

<script>
    $(document).ready(function() {
        $('body').on('click', '#setuju', function() {
            var data_id = $(this).data('id');
            $.get('/dosen/persetujuan/' + data_id + '/edit', function(data) {
                $('#setujutitle').html("Setujui Pendaftaran Judul TA");
                $('#btn-update').val("Update");
                $('#btn-save').prop('disabled', false);
                $('#edit-modal').modal('show');
                $('input[name=action]').val('setuju');
                $('#pendid').val(data.pendaftaran_id);
            })
        });
    });
    $(document).ready(function() {
        $('body').on('click', '#tolak', function() {
            var data_id = $(this).data('id');
            $.get('/dosen/persetujuan/' + data_id + '/edit', function(data) {
                $('#setujutitle').html("Tolak Pendaftaran Judul TA");
                $('#btn-update').val("Update");
                $('#btn-save').prop('disabled', false);
                $('#edit-modal').modal('show');
                $('input[name=action]').val('tolak');
                $('#pendid').val(data.pendaftaran_id);
            })
        });
    });
</script>