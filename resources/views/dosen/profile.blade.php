<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <div class="breadcrumb-wrapper">
                        <h1>Profile</h1>

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/admin/dashboard') }}">
                                        <span class="mdi mdi-home"></span> Dashboard
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    My Profile
                                </li>
                            </ol>
                        </nav>
                        @include('sweetalert::alert')
                        <div class="col-l2">
                            <div class="card card-default">
                                <div class="card-body">
                                    <form class="form" action="{{$profile->id}}/update" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="text-dark font-weight-medium" for="">NIDN</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-clipboard-account"></i>
                                                        </span>
                                                    </div>
                                                    <input id="id" type="text" class="form-control" value="{{$profile->nidn}}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="text-dark font-weight-medium" for="">Nama</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-account"></i>
                                                        </span>
                                                    </div>
                                                    <input id="name" name="name" type="text" class="form-control" value="{{$profile->name}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="text-dark font-weight-medium" for="">Alamat</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-account-location"></i>
                                                        </span>
                                                    </div>
                                                    <input id="alamat" name="alamat" type="text" class="form-control" value="{{$profile->alamat}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="text-dark font-weight-medium" for="">No. Telp</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-account-card-details"></i>
                                                        </span>
                                                    </div>
                                                    <input id="no_telp" name="no_telp" type="text" class="form-control" value="{{$profile->no_telp}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="text-dark font-weight-medium" for="">Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-email"></i>
                                                        </span>
                                                    </div>
                                                    <input id="email" name="email" type="text" class="form-control" value="{{$profile->email}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="text-dark font-weight-medium" for="">Username</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-security-account"></i>
                                                        </span>
                                                    </div>
                                                    <input id="username" name="username" type="text" class="form-control" value="{{$profile->username}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="text-dark font-weight-medium" for="">Password Baru</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-lock"></i>
                                                        </span>
                                                    </div>
                                                    <input id="password" name="password" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div style="text-align: center;">
                                                <button class="btn btn-success" onclick="return confirm('Are you sure?')">Update Profile</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')

</body>

</html>