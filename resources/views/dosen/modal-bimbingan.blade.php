<script type="text/javascript">
    $(document).ready(function() {
        $('#table_bimbingan').DataTable();
    });
</script>

<script>
    $(document).ready(function() {
        $('body').on('click', '#edit-ket', function() {
            var data_id = $(this).data('id');
            $.get('/dosen/bimbingan/' + data_id + '/edit', function(data) {
                $('#doseneditket').html("Input Komentar");
                $('#btn-update').val("Update");
                $('#btn-save').prop('disabled', false);
                $('#edit-modal').modal('show');
                $('#bimid').val(data.bimbingan_id);
                $('#bimbingan_ket').val(data.bimbingan_ket);
            })
        });
    });
</script>