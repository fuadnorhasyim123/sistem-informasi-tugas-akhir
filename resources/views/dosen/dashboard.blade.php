<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <!-- Top Statistics -->
                    <div class="row">
                        <div class="col-xl-3 col-sm-6">
                            <div class="card card-mini mb-4">
                                <div class="card-body">
                                    <h2 class="mb-1">{{$total_disetujui}}</h2>
                                    <p>Total TA Disetujui</p>
                                    <div class="chartjs-wrapper">
                                        <canvas id="barChart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6">
                            <div class="card card-mini  mb-4">
                                <div class="card-body">
                                    <h2 class="mb-1">{{$total_menunggu}}</h2>
                                    <p>Total TA Belum Disetujui</p>
                                    <div class="chartjs-wrapper">
                                        <canvas id="dual-line"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-sm-6">
                            <div class="card card-mini mb-4">
                                <div class="card-body">
                                    <h2 class="mb-1">{{$total_bimbingan}}</h2>
                                    <p>Total Bimbingan TA</p>
                                    <div class="chartjs-wrapper">
                                        <canvas id="area-chart"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')

</body>

</html>