<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('assets/img/siata.png')}}">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>SIPMTA</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
			CSS
			============================================= -->
    <link rel="stylesheet" href="welcome/css/linearicons.css">
    <link rel="stylesheet" href="welcome/css/font-awesome.min.css">
    <link rel="stylesheet" href="welcome/css/bootstrap.css">
    <link rel="stylesheet" href="welcome/css/magnific-popup.css">
    <link rel="stylesheet" href="welcome/css/nice-select.css">
    <link rel="stylesheet" href="welcome/css/hexagons.min.css">
    <link rel="stylesheet" href="welcome/css/animate.min.css">
    <link rel="stylesheet" href="welcome/css/owl.carousel.css">
    <link rel="stylesheet" href="welcome/css/main.css">
    <link href="https://cdn.materialdesignicons.com/3.0.39/css/materialdesignicons.min.css" rel="stylesheet" />
</head>

<body>

    <div id="logo">
        <!-- <a href="#"><img src="welcome/img/siata.png" alt="" title="" /></a> -->
    </div>

    <!-- start banner Area -->
    <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row fullscreen d-flex align-items-center justify-content-center">
                <div class="banner-content col-lg-12 col-md-12">
                    <h1>
                        SISTEM INFORMASI PENDAFTARAN DAN MONITORING TUGAS AKHIR
                    </h1>
                    <div style="margin-left: auto; margin-right: auto; display: block;" class="col-lg-3">
                        <a href="/login" class="btn btn-success btn-block">LOGIN <i style="float: right;" class="mdi mdi-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <script src="welcome/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="welcome/js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
    <script src="welcome/js/easing.min.js"></script>
    <script src="welcome/js/hoverIntent.js"></script>
    <script src="welcome/js/superfish.min.js"></script>
    <script src="welcome/js/jquery.ajaxchimp.min.js"></script>
    <script src="welcome/js/jquery.magnific-popup.min.js"></script>
    <script src="welcome/js/owl.carousel.min.js"></script>
    <script src="welcome/js/hexagons.min.js"></script>
    <script src="welcome/js/jquery.nice-select.min.js"></script>
    <script src="welcome/js/jquery.counterup.min.js"></script>
    <script src="welcome/js/waypoints.min.js"></script>
    <script src="welcome/js/mail-script.js"></script>
    <script src="welcome/js/main.js"></script>
</body>

</html>