<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <div class="breadcrumb-wrapper">
                        <h1>Data Bimbingan TA</h1>

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/koor-ta/dashboard') }}">
                                        <span class="mdi mdi-home"></span> Dashboard
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    Bimbingan TA
                                </li>
                            </ol>
                        </nav>

                    </div>
                    @include('sweetalert::alert')
                    <div class="col-l2">
                        <div class="card card-default">
                            <div class="card-body">
                                <form class="form">
                                    <div class="form-body">
                                        <div class="table-responsive">
                                            <table id="table_bimbingan" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">No.</th>
                                                        <th scope="col">NIM</th>
                                                        <th scope="col">Nama Mahasiswa</th>
                                                        <th scope="col">Dosen Pembimbing</th>
                                                        <th scope="col">Judul TA</th>
                                                        <th scope="col">Jumlah Bimbingan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach ($bimbingan as $data)
                                                    <tr>
                                                        <td style="text-align: center;">{{$no++}}</td>
                                                        <td>{{ $data->nim }}</td>
                                                        <td>{{ $data->nama_mhs }}</td>
                                                        <td>{{ $data->nama_dosen }}</td>
                                                        <td>{{ $data->pendaftaran_judul }}</td>
                                                        <td>{{ $data->jml_bimbingan }}</td>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#table_bimbingan').DataTable();
        });
    </script>

</body>

</html>