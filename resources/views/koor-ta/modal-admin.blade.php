<script type="text/javascript">
    $(document).ready(function() {
        $('#table_admin').DataTable();
    });
</script>

<script>
    $(document).ready(function() {

        /* When click Add status-perkawinan button */
        $('#new-admin').click(function() {
            $('#btn-save').val("create-admin");
            $('#admin').trigger("reset");
            $('#admincrudmodal').html("Tambah Admin");
            $('#crud-modal').modal('show');
            $('#admin_username').val("");
            $('#admin_nama').val("");
            $('#admin_alamat').val("");
            $('#admin_no_telp').val("");
            $('#admin_email').val("");
            $('#admin_password').val("");
        });

        /* Edit status-perkawinan */
        $('body').on('click', '#edit-admin', function() {
            var data_id = $(this).data('id');
            $.get('/koor-ta/admin/' + data_id + '/edit', function(data) {
                $('#admineditmodal').html("Reset Password Admin");
                $('#btn-update').val("Update");
                $('#btn-save').prop('disabled', false);
                $('#edit-modal').modal('show');
                $('#adminid1').val(data.id);
            })
        });

    });
</script>