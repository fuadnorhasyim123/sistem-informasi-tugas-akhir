<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <div class="breadcrumb-wrapper">
                        <h1>Data Admin</h1>

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/koor-ta/dashboard') }}">
                                        <span class="mdi mdi-home"></span> Dashboard
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    Admin
                                </li>
                            </ol>
                        </nav>

                    </div>
                    @include('sweetalert::alert')
                    <div class="col-l2">
                        <div class="card card-default">
                            <div class="card-body">

                                <form class="form">
                                    <div class="form-body">
                                        <a href="javascript:void(0)" class="btn btn-primary mb-3" id="new-admin" data-toggle="modal" style="margin-top: -10px;">
                                            <i class="mdi mdi-plus-box-outline"></i> Tambah
                                        </a>
                                        @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        <div class="table-responsive">
                                            <table id="table_admin" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">No.</th>
                                                        <th scope="col">ID Admin</th>
                                                        <th scope="col">Nama</th>
                                                        <th scope="col">Alamat</th>
                                                        <th scope="col">No. Telp.</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach ($admin as $data)
                                                    <tr>
                                                        <td style="text-align: center;">{{$no++}}</td>
                                                        <td>{{ $data->id }}</td>
                                                        <td>{{ $data->name }}</td>
                                                        <td>{{ $data->alamat }}</td>
                                                        <td>{{ $data->no_telp }}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" class="btn btn-warning" id="edit-admin" data-toggle="modal" data-id="{{ $data->id }} "><i class="mdi mdi-square-edit-outline"></i></a>
                                                            <meta name="csrf-token" content="{{ csrf_token() }}">
                                                            <a href="admin/hapus/{{$data->id}}" class="btn btn-danger delete-user" onclick="return confirm('Are you sure?')"><i class="mdi mdi-trash-can-outline"></i></a>
                                                        </td>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal fade" id="crud-modal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="admincrudmodal"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form name="custForm" action="admin/tambah" method="POST">
                                            <input type="hidden" name="adminid" id="adminid">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="nama">Nama</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-account"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="admin_nama" id="admin_nama" class="form-control" onchange="validate()" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="alamat">Alamat</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-account-location"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="admin_alamat" id="admin_alamat" class="form-control" onchange="validate()" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="no_telp">No. Telp</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-account-card-details"></i>
                                                                </span>
                                                            </div>
                                                            <input type="number" name="admin_no_telp" id="admin_no_telp" class="form-control" onchange="validate()" minlength="12" maxlength="12" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-email"></i>
                                                                </span>
                                                            </div>
                                                            <input type="email" name="admin_email" id="admin_email" class="form-control" onchange="validate()" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="username">Username</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-security-account"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="admin_username" id="admin_username" class="form-control" onchange="validate()" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="password">Password</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-lock"></i>
                                                                </span>
                                                            </div>
                                                            <input type="password" name="admin_password" id="admin_password" class="form-control" onchange="validate()" minlength="8" maxlength="15" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Submit</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalTooltip" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="admineditmodal"></h5>
                                    </div>
                                    <div class="modal-body">
                                        <form name="custForm" action="admin/edit/" method="POST">
                                            <input type="hidden" name="adminid1" id="adminid1">
                                            @csrf
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Reset</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')
    @include('koor-ta.modal-admin')

</body>

</html>