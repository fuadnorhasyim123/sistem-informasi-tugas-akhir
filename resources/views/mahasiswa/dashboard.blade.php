<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
    <style>
        .page-wrapper {
            background-color: #DAECFF;
        }

        img.logo {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        div.panduan {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        #selamat {
            text-align: center;
        }

        #nama {
            text-align: center;
        }
    </style>
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <img class="logo" src="{{asset('assets/img/logo_poltek.png')}}" alt="Logo" width="200" height="200">
            <h4 id="nama"><b>{{ Auth::user()->name }}</b></h4>
            <br>
            <h5 id="selamat">SELAMAT DATANG Di SIPMTA</h5>
            <p id="selamat">(Sistem Informasi Pendaftaran Dan Monitoring Tugas Akhir)</p>
            <br>
            <br>

            <div class="col-10 panduan">
                <div class="card card-default">
                    <div class="card-header justify-content-between card-header-border-bottom">
                        <p><i class="mdi mdi-message-text-outline"></i>&emsp;Panduan :</p>
                    </div>
                    <div class="card-body">
                        <div class="note note-info">
                            <h5>Pendaftaran TA :</h5>
                            <ul>
                                <li> &emsp; <i class="mdi mdi-circle-medium"></i> Mahasiswa harus menginputkan judul TA, memilih dosen pembimbing dan mengupload berkas untuk Tugas Akhir</li>
                                <li> &emsp; <i class="mdi mdi-circle-medium"></i> Mahasiswa menunggu konfirmasi persetujuan pendaftaran judul TA dari dosen pembimbing TA </li>
                            </ul>
                            <br>
                            <h5>Bimbingan TA :</h5>
                            <ul>
                                <li> &emsp; <i class="mdi mdi-circle-medium"></i> Mahasiswa Dapat menambah bimbingan TA sesuai jadwal dengan dosen pembimbing TA</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>
    @include('layouts.script')

</body>

</html>