<script type="text/javascript">
    $(document).ready(function() {
        $('#table_bimbingan').DataTable();
    });
</script>

<script>
    $(document).ready(function() {

        /* When click Add status-perkawinan button */
        $('#new-bimbingan').click(function() {
            $('#btn-save').val("create-bimbingan");
            $('#bimbingan').trigger("reset");
            $('#bimbingancrudmodal').html("Tambah Bimbingan TA");
            $('#crud-modal').modal('show');
        });

    });
</script>

<script>
    $(document).ready(function() {
        $(document).on('click', '#upload', function(e) {
            $('#modalupload').modal('show');
            $('#formberkas').trigger("reset");
            $('#title').html("Upload Berkas");

        });

    });
</script>