<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <div class="breadcrumb-wrapper">
                        <h1>Data Pendaftaran TA</h1>

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/mahasiswa/dashboard') }}">
                                        <span class="mdi mdi-home"></span> Dashboard
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    Pendaftaran TA
                                </li>
                            </ol>
                        </nav>
                    </div>
                    @include('sweetalert::alert')
                    <div class="col-lg-12">
                        <div class="card card-default">
                            <div class="card-body">
                                <form>
                                    @if(empty($pendaftaran))
                                    <a href="javascript:void(0)" class="btn btn-primary mb-3" id="input-judul" data-toggle="modal" style="margin-top: -10px;">
                                        <i class="mdi mdi-plus-box-outline"></i> Input
                                    </a>
                                    @else
                                    @foreach($pendaftaran as $data)
                                    <a href="javascript:void(0)" class="btn btn-primary mb-3" id="update-judul" data-toggle="modal" data-id="{{$data->pendaftaran_id}}" style="margin-top: -10px;">
                                        <i class="mdi mdi-plus-box-outline"></i> Update
                                    </a>
                                    @endforeach
                                    @endif

                                    <a href="/document/05.-Proposal-Activity-Control-Bimbingan-Pra-Proposal-MI-PSDKU.docx" class="btn btn-success mb-1" style="margin-top: -10px; float:right;">
                                        <i class="mdi mdi-plus-box-outline"></i> Lembar Bimbingan
                                    </a>
                                    <a href="/document/04.-Proposal-Form-Kesediaan-Membimbing-Proposal-MI-PSDKU.docx" class="btn btn-success mb-3 mr-1" style="margin-top: -10px; float:right;">
                                        <i class="mdi mdi-plus-box-outline"></i> Surat Kesediaan Membimbing
                                    </a>
                                    <!-- <button type="button" class="mb-1 btn btn-sm btn-primary" name="upload" id="upload"><i class="mdi mdi-plus mr-1"></i>upload Berkas </button> -->
                                    @if(empty($pendaftaran))
                                    <table class="table table-striped" id="table_mhs">
                                        <tbody>
                                            <tr>
                                                <th scope="col">NIM</th>
                                                <td width="4">:</td>
                                                <td scope="row">-</td>
                                            </tr>
                                            <tr>
                                                <th scope="col">Nama Mahasiswa</th>
                                                <td width="4">:</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <th scope="col">Dosen Pembimbing</th>
                                                <td width="4">:</td>
                                                <td>-</td>
                                            </tr>
                                            <tr>
                                                <th scope="col">Judul</th>
                                                <td width="4">:</td>
                                                <td>-</td>
                                            </tr>
                                            <tr id="berkas">
                                                <th scope="col">Berkas</th>
                                                <td width="4">:</td>
                                                <td>
                                                    <i class="mdi mdi-circle-medium"></i> Surat Kesediaan Membimbing : <span class="badge badge-danger ml-1"><i class="mdi mdi-window-minimize"></i> Belum Terisi</span>
                                                    <i class="mdi mdi-circle-medium"></i> Lembar Bimbingan : <span class="badge badge-danger ml-1"><i class="mdi mdi-window-minimize"></i> Belum Terisi</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="col">Status</th>
                                                <td width="4">:</td>
                                                <td><span class="badge badge-danger">-</span></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    @else
                                    @foreach($pendaftaran as $data)
                                    <table class="table table-striped" id="table_mhs">
                                        <tbody>
                                            <tr>
                                                <th scope="col">NIM</th>
                                                <td width="4">:</td>
                                                <td scope="row">{{$data->nim}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="col">Nama Mahasiswa</th>
                                                <td width="4">:</td>
                                                <td>{{$data->nama_mhs}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="col">Dosen Pembimbing</th>
                                                <td width="4">:</td>
                                                <td>{{$data->nama_dosen}}</td>
                                            </tr>
                                            <tr>
                                                <th scope="col">Judul</th>
                                                <td width="4">:</td>
                                                <td>{{$data->pendaftaran_judul}}</td>
                                            </tr>
                                            <tr id="berkas">
                                                <th scope="col">Berkas</th>
                                                <td width="4">:</td>
                                                <td>
                                                    @foreach($berkas as $value)
                                                    @if($value->berkas1==null && $value->berkas2==null)
                                                    <i class="mdi mdi-circle-medium"></i> Surat Kesediaan Membimbing : <span class="badge badge-danger ml-2"><i class="mdi mdi-window-minimize"></i> Belum Terisi</span><br>
                                                    <i class="mdi mdi-circle-medium"></i> Lembar Bimbingan : <span class="badge badge-danger ml-2 mt-1"><i class="mdi mdi-window-minimize"></i> Belum Terisi</span>
                                                    @else
                                                    <i class="mdi mdi-circle-medium"></i> Surat Kesediaan Membimbing : {{$value->berkas1}}<span class="badge badge-success ml-2"><i class="mdi mdi-check"></i> Sudah Terisi</span><br>
                                                    <i class="mdi mdi-circle-medium"></i> Lembar Bimbingan : {{$value->berkas2}}<span class="badge badge-success ml-2 mt-1"><i class="mdi mdi-check"></i> Sudah Terisi</span>
                                                    @endif
                                                    @endforeach
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="col">Status</th>
                                                <td width="4">:</td>
                                                <td>@if($data->pendaftaran_status=='Menunggu Persetujuan')<span class="badge badge-warning">{{$data->pendaftaran_status}}</span>@elseif($data->pendaftaran_status=='Ditolak')<span class="badge badge-danger">{{$data->pendaftaran_status}}</span>@else<span class="badge badge-success">{{$data->pendaftaran_status}}</span>@endif </td>
                                            </tr>
                                            <tr>
                                                @foreach($berkas as $value)
                                                @if($value->berkas1==null && $value->berkas2==null)
                                                <a href="javascript:void(0)" class="btn btn-secondary mb-3" name="upload" id="upload" data-toggle="modal" style="margin-top: -10px;">
                                                    <i class="mdi mdi-plus-box-outline"></i> Upload Berkas
                                                </a>
                                                @else
                                                <a href="hapus/{{$data->nim}}" class="btn btn-secondary mb-3" style="margin-top: -10px;" onclick="return confirm('Are you sure?')">
                                                    <i class="mdi mdi-plus-box-outline"></i> Hapus Berkas
                                                </a>
                                                @endif
                                                @endforeach
                                            </tr>
                                        </tbody>
                                    </table>
                                    @endforeach
                                    @endif
                                </form>
                            </div>
                        </div>
                        <!-- modal input judul -->
                        <div class="modal fade" id="crud-modal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="pendaftarancrudmodal"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form name="custForm" action="{{ Auth::user()->id }}/tambah" method="POST">
                                            <input type="hidden" id="pendid" name="pendid" value="">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="nim">NIM</label>
                                                        <div class="position-relative has-icon-left">
                                                            @foreach($nim as $data)
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="mdi mdi-clipboard-account"></i>
                                                                    </span>
                                                                </div>
                                                                <input type="text" name="nim" id="nim" class="form-control" value="{{$data->nim}}" onchange="validate()" readonly>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlSelect12">Pilih Dosen :</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-teach"></i>
                                                                </span>
                                                            </div>
                                                            <select class="form-control" id="nidn" name="nidn">
                                                                <option>---</option>
                                                                @foreach($dosen as $data)
                                                                <option value="{{$data->nidn}}">{{$data->nama_dosen}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="jdl">Input Judul TA</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="mdi mdi-school"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="pendaftaran_judul" id="pendaftaran_judul" class="form-control" onchange="validate()">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Submit</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal upload berkas 1 -->
                        <div class="modal fade" id="modalupload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="title">Upload Berkas</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ Auth::user()->id }}/uploadBerkas" method="POST" enctype="multipart/form-data" id="formberkas">
                                            {{ csrf_field() }}
                                            @foreach($pendaftaran as $data)
                                            <input type="hidden" name="nim" id="nim" value="{{$data->nim}}" class="form-control">
                                            @endforeach
                                            <div class="form-group">
                                                <label for="exampleFormControlFile1">Berkas Pendaftaran TA</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-file"></i>
                                                        </span>
                                                    </div>
                                                    <input type="file" name="berkas1" id="berkas1" value="" class="form-control" accept=".doc,.docx,.pdf/" required><br>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlFile1">Berkas Bimbingan TA</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="mdi mdi-file"></i>
                                                        </span>
                                                    </div>
                                                    <input type="file" name="berkas2" id="berkas2" value="" class="form-control" accept=".doc,.docx,.pdf/" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                    <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Submit</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')
    @include('mahasiswa.modal-pendaftaran')

</body>

</html>