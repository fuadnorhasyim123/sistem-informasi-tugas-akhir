<script type="text/javascript">
    $(document).ready(function() {
        $('#table_pendaftaran').DataTable();
    });
</script>

<script>
    $(document).ready(function() {

        /* When click Add status-perkawinan button */
        $('#input-judul').click(function() {
            $('#btn-save').val("create-pendaftaran");
            $('#pendaftaran').trigger("reset");
            $('#pendaftarancrudmodal').html("Input Pendaftaran TA");
            $('#crud-modal').modal('show');
            $('#pendaftaran_judul').val("");
            $('#pendaftaran_nidn').val("---");
        });

        $(document).ready(function() {
            $('body').on('click', '#update-judul', function() {
                var data_id = $(this).data('id');
                $.get('/mahasiswa/pendaftaran/' + data_id + '/edit', function(data) {
                    $('#pendaftarancrudmodal').html("Update Pendaftaran TA");
                    $('#btn-update').val("Update");
                    $('#btn-save').prop('disabled', false);
                    $('#crud-modal').modal('show');
                    $('#pendid').val(data.pendaftaran_id);
                    $('#nim').val(data.nim);
                    $('#nidn').val(data.nidn);
                    $('#pendaftaran_judul').val(data.pendaftaran_judul);
                })
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $(document).on('click', '#upload', function(e) {
            $('#modalupload').modal('show');
            $('#formberkas').trigger("reset");
            $('#title').html("Upload Berkas");

        });

    });
</script>