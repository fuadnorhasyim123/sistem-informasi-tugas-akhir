<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('layouts.header')
</head>


<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
        NProgress.configure({
            showSpinner: false
        });
        NProgress.start();
    </script>

    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">

        @include('layouts.sidebar')

        <div class="page-wrapper">

            @include('layouts.navbar')

            <div class="content-wrapper">
                <div class="content">
                    <div class="breadcrumb-wrapper">
                        <h1>Data Bimbingan TA</h1>

                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb p-0">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('/mahasiswa/dashboard') }}">
                                        <span class="mdi mdi-home"></span> Dashboard
                                    </a>
                                </li>
                                <li class="breadcrumb-item">
                                    Bimbingan TA
                                </li>
                            </ol>
                        </nav>
                        @include('sweetalert::alert')
                        <div class="col-l2">
                            <div class="card card-default">
                                <div class="card-body">
                                    <form class="form">
                                        <div class="form-body">
                                            @if($jdl==null)
                                            <button class="btn btn-primary mb-1" style="margin-top: -10px;" disabled>
                                                <i class="mdi mdi-plus-box-outline"></i> Tambah
                                            </button>
                                            <br>
                                            <div class="alert alert-danger" role="alert">
                                                <i class="mdi mdi-alert mr-1 mt-1 mb-3"></i> Pendaftaran TA Belum Disetujui
                                            </div>
                                            @else
                                            <a href="javascript:void(0)" class="btn btn-primary mb-3" id="new-bimbingan" data-toggle="modal" style="margin-top: -10px;">
                                                <i class="mdi mdi-plus-box-outline"></i> Tambah
                                            </a>
                                            @endif
                                            @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                            <div class="table-responsive">
                                                <table id="table_bimbingan" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col" width="3">No.</th>
                                                            <th scope="col" style="width: 300px; text-align: center;">Tanggal Bimbingan</th>
                                                            <th scope="col" style="text-align: center;">Keterangan</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $no = 1; ?>
                                                        @foreach ($bimbingan as $data)
                                                        <tr>
                                                            <td style="text-align: center;">{{$no++}}</td>
                                                            <td style="text-align: center;">{{ \Carbon\Carbon::parse($data->bimbingan_tgl)->format('d-m-Y') }}</td>
                                                            <td style="text-align: center;">{{ $data->bimbingan_ket }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="modal fade" id="crud-modal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="bimbingancrudmodal"></h4>
                                        </div>
                                        <div class="modal-body">
                                            <form name="custForm" action="{{ Auth::user()->id }}/tambah" method="POST">
                                                @csrf
                                                @foreach($jdl as $data)
                                                <input type="hidden" name="pendaftaran_id" id="pendaftaran_id" value="{{$data->pendaftaran_id}}">
                                                @endforeach
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="nim">Judul TA</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="mdi mdi-school"></i>
                                                                    </span>
                                                                </div>
                                                                @foreach($jdl as $data)
                                                                <input type="text" name="jdl" id="jdl" class="form-control" value="{{$data->pendaftaran_judul}}" onchange="validate()" readonly>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="nim">Dosen Pembimbing</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="mdi mdi-teach"></i>
                                                                    </span>
                                                                </div>
                                                                @foreach($jdl as $data)
                                                                <input type="text" name="dosbim" id="dosbim" class="form-control" value="{{$data->nama_dosen}}" onchange="validate()" readonly>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="jdl">Input Tanggal Bimbingan</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">
                                                                        <i class="mdi mdi-calendar"></i>
                                                                    </span>
                                                                </div>
                                                                <input type="date" name="bimbingan_tgl" id="bimbingan_tgl" class="form-control" onchange="validate()">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                        <button type="submit" id="btn-save" name="btnsave" class="btn btn-primary">Submit</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer mt-auto">
                @include('layouts.footer')
            </footer>

        </div>
    </div>

    @include('layouts.script')
    @include('mahasiswa.modal-bimbingan')

</body>

</html>