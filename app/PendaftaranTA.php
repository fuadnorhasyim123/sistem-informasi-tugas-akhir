<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendaftaranTA extends Model
{
    protected $table = 'pendaftaran_ta';
    protected $primaryKey = 'pendaftaran_id';
    protected $fillable = ['pendaftaran_id', 'nim', 'nidn', 'pendaftaran_judul', 'pendaftaran_status'];
}
