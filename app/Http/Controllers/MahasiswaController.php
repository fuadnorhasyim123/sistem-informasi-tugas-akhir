<?php

namespace App\Http\Controllers;

use App\Mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Imports\MahasiswaImport;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
use App\Berkas;
use App\BimbinganTA;
use App\PendaftaranTA;
use Validator;
use File;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function dashboard()
    {
        $total_mhs = \App\User::where('level', 'mahasiswa')->count();
        $total_dosen = \App\User::where('level', 'dosen')->count();
        $total_koorta = \App\User::where('level', 'koordinator')->count();
        $total_user = \App\User::all()->count();

        return view('/mahasiswa/dashboard', [
            'total_mhs' => $total_mhs,
            'total_dosen' => $total_dosen,
            'total_koorta' => $total_koorta,
            'total_user' => $total_user
        ]);
    }

    public function indexPendaftaran($id)
    {
        $pendaftaran = DB::select("SELECT aaaaa.*, bbbbb.name AS nama_dosen FROM (
                                        SELECT aaaa.*,bbbb.name AS nama_mhs FROM (
                                            SELECT aaa.*, bbb.users_id AS users_id_dosen FROM (
                                                SELECT aa.*,bb.users_id AS users_id_mhs FROM (
                                                    SELECT a.pendaftaran_id,a.nim,a.nidn,a.pendaftaran_judul,a.pendaftaran_status FROM pendaftaran_ta a, mahasiswa b WHERE a.nim=b.nim
                                                )aa, mahasiswa bb WHERE aa.nim=bb.nim
                                            ) aaa,dosen bbb WHERE aaa.nidn=bbb.nidn
                                        )aaaa, users bbbb WHERE aaaa.users_id_mhs=bbbb.id
                                    )aaaaa, users bbbbb WHERE aaaaa.users_id_dosen=bbbbb.id AND aaaaa.users_id_mhs='$id'");

        $dosen = DB::select("SELECT a.nidn, b.name AS nama_dosen FROM dosen a, users b WHERE a.users_id=b.id AND b.level='dosen'");
        $berkas = DB::select("SELECT a.berkas1, a.berkas2 FROM berkas a, mahasiswa b, users c WHERE a.nim=b.nim AND b.users_id=c.id AND id=$id");
        $nim = DB::table('mahasiswa')->where('users_id', $id)->get();

        return view('/mahasiswa/pendaftaran', [
            'pendaftaran' => $pendaftaran, 'dosen' => $dosen, 'nim' => $nim, 'berkas' => $berkas
        ]);
    }

    public function indexBimbingan($id)
    {
        $bimbingan = DB::select("SELECT aaaaa.*, bbbbb.name AS nama_dosen FROM (
                                    SELECT aaaa.*,bbbb.name AS nama_mhs FROM (
                                        SELECT aaa.*, bbb.users_id AS users_id_dosen FROM (
                                            SELECT aa.*,bb.users_id AS users_id_mhs FROM (
                                                SELECT a.bimbingan_id,a.pendaftaran_id,a.bimbingan_tgl,a.bimbingan_ket,a.created_at,b.nim,b.nidn,b.pendaftaran_judul FROM bimbingan_ta a, pendaftaran_ta b WHERE a.pendaftaran_id=b.pendaftaran_id
                                            )aa, mahasiswa bb WHERE aa.nim=bb.nim
                                        ) aaa,dosen bbb WHERE aaa.nidn=bbb.nidn
                                    )aaaa, users bbbb WHERE aaaa.users_id_mhs=bbbb.id
                                )aaaaa, users bbbbb WHERE aaaaa.users_id_dosen=bbbbb.id AND aaaaa.users_id_mhs='$id' ORDER BY created_at DESC");

        $jdl = DB::select("SELECT aaaaa.*, bbbbb.name AS nama_dosen FROM (
                                    SELECT aaaa.*,bbbb.name AS nama_mhs FROM (
                                        SELECT aaa.*, bbb.users_id AS users_id_dosen FROM (
                                            SELECT aa.*,bb.users_id AS users_id_mhs FROM (
                                                SELECT a.pendaftaran_id,a.nim,a.nidn,a.pendaftaran_judul,a.pendaftaran_status FROM pendaftaran_ta a, mahasiswa b WHERE a.nim=b.nim
                                            )aa, mahasiswa bb WHERE aa.nim=bb.nim
                                        ) aaa,dosen bbb WHERE aaa.nidn=bbb.nidn
                                    )aaaa, users bbbb WHERE aaaa.users_id_mhs=bbbb.id
                                )aaaaa, users bbbbb WHERE aaaaa.users_id_dosen=bbbbb.id AND aaaaa.users_id_mhs='$id' AND aaaaa.pendaftaran_status='Disetujui'");

        return view('/mahasiswa/bimbingan', [
            'bimbingan' => $bimbingan, 'jdl' => $jdl
        ]);
    }

    public function updateProfile(Request $request, $id)
    {
        if ($request->password == "") {
            DB::table('users')->where('id', $id)->update([
                'username' => $request->username,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'email' => $request->email
            ]);
        } else {
            DB::table('users')->where('id', $id)->update([
                'username' => $request->username,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
        }

        Alert::success('Sukses', 'Berhasil Update Profile');

        return redirect("/mahasiswa/profile/$id");
    }


    public function profile($id)
    {
        $profile = DB::table('users')->join('mahasiswa', 'users.id', '=', 'mahasiswa.users_id')
            ->select('users.*', 'mahasiswa.*')->where('id', $id)->first();

        return view('/mahasiswa/profile', [
            'profile' => $profile
        ]);
    }

    public function storePendaftaran(Request $request, $id)
    {
        $pendid = $request->pendid;
        PendaftaranTA::UpdateOrCreate(
            ['pendaftaran_id' => $pendid],
            [
                'nim' => $request->nim,
                'nidn' => $request->nidn,
                'pendaftaran_judul' => $request->pendaftaran_judul,
                'pendaftaran_status' => 'Menunggu Persetujuan'
            ]
        );

        if (empty($request->pendid)) {
            Berkas::Create(
                [
                    'nim' => $request->nim
                ]
            );

            Alert::success('Sukses', 'Berhasil Input Pendaftaran TA');
        } else {
            Alert::success('Sukses', 'Berhasil Update Pendaftaran TA');
        }

        return redirect("/mahasiswa/pendaftaran/$id");
    }

    public function storeBimbingan(Request $request, $id)
    {

        BimbinganTA::Create(
            [
                'pendaftaran_id' => $request->pendaftaran_id,
                'bimbingan_tgl' => $request->bimbingan_tgl
            ]
        );

        Alert::success('Sukses', 'Berhasil Tambah Bimbingan TA');

        return redirect("/mahasiswa/bimbingan/$id");
    }

    public function editPendaftaran($id)
    {
        $pendaftaran = DB::table('pendaftaran_ta')->where('pendaftaran_id', $id)->first();

        return Response::json($pendaftaran);
    }

    public function uploadBerkas($id, Request $request)
    {
        $file1 = $request->file('berkas1');
        $file2 = $request->file('berkas2');
        if ($file1 != NULL) {
            $nama_file1 = $file1->getClientOriginalName();
            $nama_file2 = $file2->getClientOriginalName();
            $tujuan_upload = 'document_mahasiswa';
            $file1->move($tujuan_upload, $nama_file1);
            $file2->move($tujuan_upload, $nama_file2);
            Berkas::UpdateOrCreate(
                [
                    'nim' => $request->nim
                ],
                [
                    'berkas1' => $nama_file1,
                    'berkas2' => $nama_file2
                ]
            );

            Alert::success('Sukses', 'Berhasil Upload Berkas Pendaftaran');
        }

        return redirect("mahasiswa/pendaftaran/$id");
    }

    public function destroyBerkas($id)
    {
        // hapus file
        $berkas = Berkas::where('nim', $id)->first();
        File::delete(['document_mahasiswa/' . $berkas->berkas1, 'document_mahasiswa/' . $berkas->berkas2]);

        DB::table('berkas')->where('nim', $id)->update([
            'berkas1' => "",
            'berkas2' => ""
        ]);

        Alert::success('Sukses', 'Berhasil Hapus Berkas');

        return redirect()->back();
    }
}
