<?php

namespace App\Http\Controllers;

use App\Dosen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Imports\KoorTAImport;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
use Validator;

class KoorTAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function dashboard()
    {
        $total_pendaftaran = \App\PendaftaranTA::where('pendaftaran_status', 'Disetujui')->count();
        $total_bimbingan = \App\BimbinganTA::all()->count();
        $total_mhs = \App\User::where('level', 'mahasiswa')->count();
        $total_dosen = \App\User::where('level', 'dosen')->count();

        return view('/koor-ta/dashboard', [
            'total_pendaftaran' => $total_pendaftaran,
            'total_bimbingan' => $total_bimbingan,
            'total_mhs' => $total_mhs,
            'total_dosen' => $total_dosen
        ]);
    }

    public function indexAdmin()
    {
        $admin = DB::table('users')->where('level', '=', 'Admin')->get();

        return view('/koor-ta/admin', [
            'admin' => $admin
        ]);
    }

    public function indexPendaftaran()
    {
        $pendaftaran = DB::select("SELECT aaaaa.*, bbbbb.name AS nama_dosen FROM (
                                        SELECT aaaa.*,bbbb.name AS nama_mhs FROM (
                                            SELECT aaa.*, bbb.users_id AS users_id_dosen FROM (
                                                SELECT aa.*,bb.users_id AS users_id_mhs FROM (
                                                    SELECT a.pendaftaran_id,a.nim,a.nidn,a.pendaftaran_judul,a.pendaftaran_status,a.created_at,c.berkas1, c.berkas2 FROM pendaftaran_ta a, mahasiswa b, berkas c WHERE a.nim=b.nim AND b.nim=c.nim
                                                )aa, mahasiswa bb WHERE aa.nim=bb.nim
                                            ) aaa,dosen bbb WHERE aaa.nidn=bbb.nidn
                                        )aaaa, users bbbb WHERE aaaa.users_id_mhs=bbbb.id
                                    )aaaaa, users bbbbb WHERE aaaaa.users_id_dosen=bbbbb.id AND pendaftaran_status='Disetujui' ORDER BY created_at DESC");

        return view('/koor-ta/pendaftaran', [
            'pendaftaran' => $pendaftaran
        ]);
    }

    public function indexBimbingan()
    {
        $bimbingan = DB::select("SELECT COUNT(*) as jml_bimbingan,aaaaa.*, bbbbb.name AS nama_dosen FROM (
                                    SELECT aaaa.*,bbbb.name AS nama_mhs FROM (
                                        SELECT aaa.*, bbb.users_id AS users_id_dosen FROM (
                                            SELECT aa.*,bb.users_id AS users_id_mhs FROM (
                                                SELECT a.bimbingan_id,a.pendaftaran_id,a.bimbingan_tgl,a.bimbingan_ket,a.created_at,b.nim,b.nidn,b.pendaftaran_judul FROM bimbingan_ta a, pendaftaran_ta b WHERE a.pendaftaran_id=b.pendaftaran_id
                                            )aa, mahasiswa bb WHERE aa.nim=bb.nim
                                        ) aaa,dosen bbb WHERE aaa.nidn=bbb.nidn
                                    )aaaa, users bbbb WHERE aaaa.users_id_mhs=bbbb.id
                                )aaaaa, users bbbbb WHERE aaaaa.users_id_dosen=bbbbb.id GROUP BY pendaftaran_id ORDER BY created_at DESC");

        return view('/koor-ta/bimbingan', [
            'bimbingan' => $bimbingan
        ]);
    }

    public function storeAdmin(Request $request)
    {

        $rules = [
            'admin_email'                 => 'required|email|unique:users,email',
            'admin_password'              => 'required|string|min:8'
        ];

        $messages = [
            'admin_email.required'          => 'Email wajib diisi',
            'admin_email.email'             => 'Email tidak valid',
            'admin_email.unique'            => 'Email sudah terdaftar',
            'admin_password.required'       => 'Password wajib diisi',
            'admin_password.string'         => 'Password harus huruf dan angka',
            'admin_password.min'            => 'Password minimal 8 karakter'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        User::Create(
            [
                'username' => $request->admin_username,
                'name' => $request->admin_nama,
                'alamat' => $request->admin_alamat,
                'no_telp' => $request->admin_no_telp,
                'email' => $request->admin_email,
                'password' => Hash::make($request->admin_password),
                'level' => 'admin',
            ]
        );

        Alert::success('Sukses', 'Berhasil Tambah Admin');

        return redirect("/koor-ta/admin");
    }

    public function updateAdmin(Request $request)
    {
        $id = $request->adminid1;

        User::UpdateOrCreate(
            [
                'id' => $id,
            ],
            [
                'password' => Hash::make('12345678')
            ]
        );

        Alert::success('Sukses', 'Berhasil Reset Password Admin');

        return redirect("/koor-ta/admin");
    }

    public function editAdmin($id)
    {
        $admin = DB::table('users')->where('id', $id)->first();

        return Response::json($admin);
    }

    public function destroyAdmin($id)
    {
        DB::table('users')->where('id', $id)->delete();

        Alert::success('Sukses', 'Berhasil Hapus Admin');

        return redirect("/koor-ta/admin");
    }

    public function updateProfile(Request $request, $id)
    {
        if ($request->password == "") {
            DB::table('users')->where('id', $id)->update([
                'username' => $request->username,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'email' => $request->email
            ]);
        } else {
            DB::table('users')->where('id', $id)->update([
                'username' => $request->username,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
        }

        Alert::success('Sukses', 'Berhasil Update Profile');

        return redirect("/koor-ta/profile/$id");
    }


    public function profile($id)
    {
        $profile = DB::table('users')->join('dosen', 'users.id', '=', 'dosen.users_id')
            ->select('users.*', 'dosen.*')->where('id', $id)->first();

        return view('/koor-ta/profile', [
            'profile' => $profile
        ]);
    }
}
