<?php

namespace App\Http\Controllers;

use App\Dosen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\PendaftaranTA;
use App\BimbinganTA;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Imports\DosenImport;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
use Validator;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function dashboard()
    {
        $total_menunggu = \App\PendaftaranTA::where('pendaftaran_status', 'Menunggu Persetujuan')->count();
        $total_disetujui = \App\PendaftaranTA::where('pendaftaran_status', 'Disetujui')->count();
        $total_bimbingan = \App\BimbinganTA::all()->count();

        return view('/dosen/dashboard', [
            'total_disetujui' => $total_disetujui,
            'total_menunggu' => $total_menunggu,
            'total_bimbingan' => $total_bimbingan
        ]);
    }

    public function indexPendaftaran($id)
    {
        $pendaftaran = DB::select("SELECT aaaaa.*, bbbbb.name AS nama_dosen FROM (
                                        SELECT aaaa.*,bbbb.name AS nama_mhs FROM (
                                            SELECT aaa.*, bbb.users_id AS users_id_dosen FROM (
                                                SELECT aa.*,bb.users_id AS users_id_mhs FROM (
                                                    SELECT a.pendaftaran_id,a.nim,a.nidn,a.pendaftaran_judul,a.pendaftaran_status,a.created_at, c.berkas1, c.berkas2 FROM pendaftaran_ta a, mahasiswa b, berkas c WHERE a.nim=b.nim AND b.nim=c.nim
                                                )aa, mahasiswa bb WHERE aa.nim=bb.nim
                                            ) aaa,dosen bbb WHERE aaa.nidn=bbb.nidn
                                        )aaaa, users bbbb WHERE aaaa.users_id_mhs=bbbb.id
                                    )aaaaa, users bbbbb WHERE aaaaa.users_id_dosen=bbbbb.id AND aaaaa.users_id_dosen='$id' AND pendaftaran_status='Disetujui' ORDER BY created_at DESC");

        return view('/dosen/pendaftaran', [
            'pendaftaran' => $pendaftaran
        ]);
    }

    public function indexPersetujuan($id)
    {
        $pendaftaran = DB::select("SELECT aaaaa.*, bbbbb.name AS nama_dosen FROM (
                                        SELECT aaaa.*,bbbb.name AS nama_mhs FROM (
                                            SELECT aaa.*, bbb.users_id AS users_id_dosen FROM (
                                                SELECT aa.*,bb.users_id AS users_id_mhs FROM (
                                                    SELECT a.pendaftaran_id,a.nim,a.nidn,a.pendaftaran_judul,a.pendaftaran_status,a.created_at FROM pendaftaran_ta a, mahasiswa b WHERE a.nim=b.nim
                                                )aa, mahasiswa bb WHERE aa.nim=bb.nim
                                            ) aaa,dosen bbb WHERE aaa.nidn=bbb.nidn
                                        )aaaa, users bbbb WHERE aaaa.users_id_mhs=bbbb.id
                                    )aaaaa, users bbbbb WHERE aaaaa.users_id_dosen=bbbbb.id AND aaaaa.users_id_dosen='$id' AND pendaftaran_status='Menunggu Persetujuan' ORDER BY created_at DESC");

        return view('/dosen/persetujuan', [
            'pendaftaran' => $pendaftaran
        ]);
    }

    public function indexBimbingan($id)
    {
        $bimbingan = DB::select("SELECT aaaaa.*, bbbbb.name AS nama_dosen FROM (
                                    SELECT aaaa.*,bbbb.name AS nama_mhs FROM (
                                        SELECT aaa.*, bbb.users_id AS users_id_dosen FROM (
                                            SELECT aa.*,bb.users_id AS users_id_mhs FROM (
                                                SELECT a.bimbingan_id,a.pendaftaran_id,a.bimbingan_tgl,a.bimbingan_ket,a.created_at,b.nim,b.nidn,b.pendaftaran_judul FROM bimbingan_ta a, pendaftaran_ta b WHERE a.pendaftaran_id=b.pendaftaran_id
                                            )aa, mahasiswa bb WHERE aa.nim=bb.nim
                                        ) aaa,dosen bbb WHERE aaa.nidn=bbb.nidn
                                    )aaaa, users bbbb WHERE aaaa.users_id_mhs=bbbb.id
                                )aaaaa, users bbbbb WHERE aaaaa.users_id_dosen=bbbbb.id AND aaaaa.users_id_dosen='$id' ORDER BY created_at DESC");

        return view('/dosen/bimbingan', [
            'bimbingan' => $bimbingan
        ]);
    }

    public function updateProfile(Request $request, $id)
    {
        if ($request->password == "") {
            DB::table('users')->where('id', $id)->update([
                'username' => $request->username,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'email' => $request->email
            ]);
        } else {
            DB::table('users')->where('id', $id)->update([
                'username' => $request->username,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
        }

        Alert::success('Sukses', 'Berhasil Update Profile');

        return redirect("/dosen/profile/$id");
    }


    public function profile($id)
    {
        $profile = DB::table('users')->join('dosen', 'users.id', '=', 'dosen.users_id')
            ->select('users.*', 'dosen.*')->where('id', $id)->first();

        return view('/dosen/profile', [
            'profile' => $profile
        ]);
    }

    public function editPendaftaran($id)
    {
        $pendaftaran = DB::table('pendaftaran_ta')->where('pendaftaran_id', $id)->first();

        return Response::json($pendaftaran);
    }

    public function updatePendaftaran($id, Request $request)
    {
        if ($request->action == 'setuju') {
            DB::table('pendaftaran_ta')
                ->where('pendaftaran_id', $request->pendid)
                ->update(['pendaftaran_status' => 'Disetujui']);
        } else if ($request->action == 'tolak') {
            DB::table('pendaftaran_ta')
                ->where('pendaftaran_id', $request->pendid)
                ->update(['pendaftaran_status' => 'Ditolak']);
        }

        Alert::success('Sukses', 'Berhasil Dikonfirmasi');

        return redirect("/dosen/persetujuan/$id");
    }

    public function editBimbingan($id)
    {
        $bimbingan = DB::table('bimbingan_ta')->where('bimbingan_id', $id)->first();

        return Response::json($bimbingan);
    }

    public function updateBimbingan($id, Request $request)
    {
        DB::table('bimbingan_ta')
            ->where('bimbingan_id', $request->bimid)
            ->update(['bimbingan_ket' => $request->bimbingan_ket]);

        Alert::success('Sukses', 'Berhasil Menambah Komentar');

        return redirect("/dosen/bimbingan/$id");
    }

    public function detailMhs($id)
    {
        $mhs = DB::table('users')->join('mahasiswa', 'users.id', '=', 'mahasiswa.users_id')->select('users.*', 'mahasiswa.*')->where('nim', $id)->first();

        return Response::json($mhs);
    }
}
