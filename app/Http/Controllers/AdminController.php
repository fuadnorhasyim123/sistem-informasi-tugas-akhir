<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Mahasiswa;
use Response;
use App\User;
use App\Dosen;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Crypt;
use App\Imports\MahasiswaImport;
use Maatwebsite\Excel\Facades\Excel;
use Alert;
use Validator;
use App\Imports\DosenImport;
use App\Imports\KoorTAImport;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function dashboard()
    {
        $total_mhs = \App\User::where('level', 'mahasiswa')->count();
        $total_dosen = \App\User::where('level', 'dosen')->count();
        $total_koorta = \App\User::where('level', 'koordinator')->count();
        $total_user = \App\User::all()->count();

        return view('/admin/dashboard', [
            'total_mhs' => $total_mhs,
            'total_dosen' => $total_dosen,
            'total_koorta' => $total_koorta,
            'total_user' => $total_user
        ]);
    }

    public function indexMhs()
    {
        $mhs = DB::select("SELECT a.*,b.* FROM users a, mahasiswa b WHERE a.id=b.users_id AND a.level = 'mahasiswa' ORDER BY a.created_at DESC");

        return view('/admin/mahasiswa', [
            'mhs' => $mhs
        ]);
    }

    public function indexDosen()
    {
        $dosen = DB::select("SELECT a.*,b.* FROM users a, dosen b WHERE a.id=b.users_id AND a.level = 'dosen' ORDER BY a.created_at DESC");

        return view('/admin/dosen', [
            'dosen' => $dosen
        ]);
    }

    public function indexKoorta()
    {
        $koorta = DB::select("SELECT a.*,b.* FROM users a, dosen b WHERE a.id=b.users_id AND a.level = 'koordinator' ORDER BY a.created_at DESC");

        return view('/admin/koor-ta', [
            'koorta' => $koorta
        ]);
    }

    public function storeMhs(Request $request)
    {

        $rules = [
            'mhs_nim'                   => 'required|unique:mahasiswa,nim'
        ];

        $messages = [
            'mhs_nim.unique'              => 'NIM sudah ada',
            'mhs_nim.required'            => 'NIM wajib diisi'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $data = User::Create(
            [
                'username' => $request->mhs_username,
                'name' => $request->mhs_nama,
                'alamat' => $request->mhs_alamat,
                'no_telp' => $request->mhs_no_telp,
                'email' => "$request->mhs_nim@student.polinema.ac.id",
                'password' => Hash::make($request->mhs_nim),
                'level' => 'mahasiswa',
            ]
        );

        $lastMhsId = $data->id;
        Mahasiswa::create(
            [
                'nim' => $request->mhs_nim,
                'users_id' => $lastMhsId,
            ]
        );

        Alert::success('Sukses', 'Berhasil Tambah Mahasiswa');

        return redirect("/admin/mahasiswa");
    }

    public function updateMhs(Request $request)
    {
        $id = $request->mhsid1;
        $nim = $request->mhsnim1;

        User::UpdateOrCreate(
            [
                'id' => $id,
            ],
            [
                'password' => Hash::make($nim)
            ]
        );

        Alert::success('Sukses', 'Berhasil Reset Password Mahasiswa');

        return redirect("/admin/mahasiswa");
    }

    public function editMhs($id)
    {
        $mhs = DB::table('users')->join('mahasiswa', 'users.id', '=', 'mahasiswa.users_id')->select('users.*', 'mahasiswa.*')->where('id', $id)->first();

        return Response::json($mhs);
    }

    public function destroyMhs($id)
    {
        DB::table('users')->where('id', $id)->delete();
        DB::table('mahasiswa')->where('users_id', $id)->delete();

        Alert::success('Sukses', 'Berhasil Hapus Mahasiswa');

        return redirect("/admin/mahasiswa");
    }

    public function importMhs(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_mahasiswa', $nama_file);

        // import data
        Excel::import(new MahasiswaImport, public_path('/file_mahasiswa/' . $nama_file));

        // notifikasi dengan session
        Alert::success('Sukses', 'Berhasil Import Mahasiswa');

        // alihkan halaman kembali
        return redirect('/admin/mahasiswa');
    }

    public function updateProfile(Request $request, $id)
    {
        if ($request->password == "") {
            DB::table('users')->where('id', $id)->update([
                'username' => $request->username,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'email' => $request->email
            ]);
        } else {
            DB::table('users')->where('id', $id)->update([
                'username' => $request->username,
                'name' => $request->name,
                'alamat' => $request->alamat,
                'no_telp' => $request->no_telp,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);
        }

        Alert::success('Sukses', 'Berhasil Update Profile');

        return redirect("/admin/profile/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function profile($id)
    {
        $profile = DB::table('users')->where('id', $id)->first();

        return view('/admin/profile', [
            'profile' => $profile
        ]);
    }

    public function storeKoorta(Request $request)
    {
        $rules = [
            'koorta_nidn'                   => 'required|unique:dosen,nidn'
        ];

        $messages = [
            'koorta_nidn.unique'              => 'NIDN sudah ada',
            'koorta_nidn.required'            => 'NIDN wajib diisi'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $data = User::Create(
            [
                'username' => $request->koorta_username,
                'name' => $request->koorta_nama,
                'alamat' => $request->koorta_alamat,
                'no_telp' => $request->koorta_no_telp,
                'email' => "$request->koorta_nidn@polinema.ac.id",
                'password' => Hash::make($request->koorta_nidn),
                'level' => 'koordinator',
            ]
        );

        $lastKoorTAId = $data->id;
        Dosen::create(
            [
                'nidn' => $request->koorta_nidn,
                'users_id' => $lastKoorTAId,
            ]
        );

        Alert::success('Sukses', 'Berhasil Tambah Koordinator');

        return redirect("/admin/koor-ta");
    }

    public function updateKoorta(Request $request)
    {
        $id = $request->koortaid1;

        User::UpdateOrCreate(
            [
                'id' => $id,
            ],
            [
                'password' => Hash::make($request->koortanidn1)
            ]
        );

        Alert::success('Sukses', 'Berhasil Reset Password Koordinator');

        return redirect("/admin/koor-ta");
    }

    public function editKoorta($id)
    {
        $koorta = DB::table('users')->join('dosen', 'users.id', '=', 'dosen.users_id')->select('users.*', 'dosen.*')->where('id', $id)->first();

        return Response::json($koorta);
    }

    public function destroyKoorta($id)
    {
        DB::table('users')->where('id', $id)->delete();
        DB::table('dosen')->where('users_id', $id)->delete();

        Alert::success('Sukses', 'Berhasil Hapus Koordinator TA');

        return redirect("/admin/koor-ta");
    }

    public function importKoorta(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_koorta', $nama_file);

        // import data
        Excel::import(new KoorTAImport, public_path('/file_koorta/' . $nama_file));

        // notifikasi dengan session
        Alert::success('Sukses', 'Berhasil Import Koordinator');

        // alihkan halaman kembali
        return redirect('/admin/koor-ta');
    }

    public function storeDosen(Request $request)
    {
        $rules = [
            'dosen_nidn'                   => 'required|unique:dosen,nidn'
        ];

        $messages = [
            'dosen_nidn.unique'             => 'NIDN sudah ada',
            'dosen_nidn.required'           => 'NIDN wajib diisi'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }

        $data = User::Create(
            [
                'username' => $request->dosen_username,
                'name' => $request->dosen_nama,
                'alamat' => $request->dosen_alamat,
                'no_telp' => $request->dosen_no_telp,
                'email' => "$request->dosen_nidn@polinema.ac.id",
                'password' => Hash::make($request->dosen_nidn),
                'level' => 'dosen',
            ]
        );

        $lastDosenId = $data->id;
        Dosen::create(
            [
                'nidn' => $request->dosen_nidn,
                'users_id' => $lastDosenId,
            ]
        );

        Alert::success('Sukses', 'Berhasil Tambah Dosen');

        return redirect("/admin/dosen");
    }

    public function updateDosen(Request $request)
    {
        $id = $request->dosenid1;

        User::UpdateOrCreate(
            [
                'id' => $id,
            ],
            [
                'password' => Hash::make($request->dosennidn1)
            ]
        );

        Alert::success('Sukses', 'Berhasil Reset Password Dosen');

        return redirect("/admin/dosen");
    }

    public function editDosen($id)
    {
        $dosen = DB::table('users')->join('dosen', 'users.id', '=', 'dosen.users_id')->select('users.*', 'dosen.*')->where('id', $id)->first();

        return Response::json($dosen);
    }

    public function destroyDosen($id)
    {
        DB::table('users')->where('id', $id)->delete();
        DB::table('dosen')->where('users_id', $id)->delete();

        Alert::success('Sukses', 'Berhasil Hapus Dosen');

        return redirect("/admin/dosen");
    }

    public function importDosen(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand() . $file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_dosen', $nama_file);

        // import data
        Excel::import(new DosenImport, public_path('/file_dosen/' . $nama_file));

        // notifikasi dengan session
        Alert::success('Sukses', 'Berhasil Import Dosen');

        // alihkan halaman kembali
        return redirect('/admin/dosen');
    }
}
