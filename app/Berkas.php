<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berkas extends Model
{
    protected $table = 'berkas';
    protected $primaryKey = 'berkas_id';
    protected $fillable = ['berkas_id', 'nim', 'berkas1', 'berkas2'];
}
