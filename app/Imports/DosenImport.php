<?php

namespace App\Imports;

use App\User;
use App\Dosen;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;

class DosenImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        $user = User::Create([
            'username' => $row[2],
            'name' => $row[3],
            'alamat' => $row[4],
            'no_telp' => $row[5],
            'email' => "$row[1]@polinema.ac.id",
            'password' => Hash::make($row[1]),
            'level' => 'dosen',
        ]);

        $lastMhsId = $user->id;
        $dosen = Dosen::Create([
            'nidn' => $row[1],
            'users_id' => $lastMhsId,
        ]);

        return ([$user, $dosen]);
    }
}
