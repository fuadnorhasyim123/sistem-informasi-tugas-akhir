<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BimbinganTA extends Model
{
    protected $table = 'bimbingan_ta';
    protected $primaryKey = 'bimbingan_id';
    protected $fillable = ['bimbingan_id', 'pendaftaran_id', 'bimbingan_tgl', 'bimbingan_ket'];
}
