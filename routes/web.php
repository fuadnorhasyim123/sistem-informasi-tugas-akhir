<?php

use App\Http\Controllers\DosenController;
use App\Http\Controllers\KoorTAController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware(['auth'])->group(function () {

    Route::prefix('/admin')->group(function () {

        //Dashboard
        Route::get('/dashboard', function () {
            $pages = 'dashboard';
            return view('admin.dashboard', compact('pages'));
        });

        Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');

        Route::get('/mahasiswa', function () {
            return view('admin.mahasiswa');
        });

        Route::get('/mahasiswa', 'AdminController@indexMhs')->name('admin.mahasiswa');
        Route::post('/mahasiswa/tambah/', 'AdminController@storeMhs');
        Route::get('/mahasiswa/{id}/edit/', 'AdminController@editMhs');
        Route::post('/mahasiswa/edit', 'AdminController@updateMhs');
        Route::get('/mahasiswa/hapus/{id}', 'AdminController@destroyMhs');
        Route::post('/mahasiswa/import/', 'AdminController@importMhs');

        Route::get('/dosen', function () {
            return view('admin.dosen');
        });

        Route::get('/dosen', 'AdminController@indexDosen')->name('admin.dosen');
        Route::post('/dosen/tambah/', 'AdminController@storeDosen');
        Route::get('/dosen/{id}/edit/', 'AdminController@editDosen');
        Route::post('/dosen/edit', 'AdminController@updateDosen');
        Route::get('/dosen/hapus/{id}', 'AdminController@destroyDosen');
        Route::post('/dosen/import/', 'AdminController@importDosen');

        Route::get('/koor-ta', function () {
            return view('admin.koor-ta');
        });

        Route::get('/koor-ta', 'AdminController@indexKoorta')->name('admin.koorta');
        Route::post('/koor-ta/tambah/', 'AdminController@storeKoorta');
        Route::get('/koor-ta/{id}/edit/', 'AdminController@editKoorta');
        Route::post('/koor-ta/edit', 'AdminController@updateKoorta');
        Route::get('/koor-ta/hapus/{id}', 'AdminController@destroyKoorta');
        Route::post('/koor-ta/import/', 'AdminController@importKoorta');

        Route::get('/profile/{id}', 'AdminController@profile');
        Route::post('/profile/{id}/update', 'AdminController@updateProfile');
    });

    return view('welcome');
});

Route::middleware(['auth'])->group(function () {

    Route::prefix('/dosen')->group(function () {

        //Dashboard
        Route::get('/dashboard', function () {
            return view('dosen.dashboard');
        });

        Route::get('/dashboard', 'DosenController@dashboard')->name('dosen.dashboard');

        Route::get('/pendaftaran/{id}', function () {
            return view('dosen.pendaftaran');
        });

        Route::get('/pendaftaran/{id}', 'DosenController@indexPendaftaran')->name('dosen.pendaftaran');
        Route::get('/pendaftaran/{id}/detail/', 'DosenController@detailMhs');

        Route::get('/persetujuan/{id}', function () {
            return view('dosen.persetujuan');
        });

        Route::get('/persetujuan/{id}', 'DosenController@indexPersetujuan')->name('dosen.persetujuan');
        Route::get('/persetujuan/{id}/edit/', 'DosenController@editPendaftaran');
        Route::post('/persetujuan/{id}/update', 'DosenController@updatePendaftaran');

        Route::get('/bimbingan/{id}', function () {
            return view('dosen.bimbingan');
        });

        Route::get('/bimbingan/{id}', 'DosenController@indexBimbingan')->name('dosen.bimbingan');
        Route::get('/bimbingan/{id}/edit/', 'DosenController@editBimbingan');
        Route::post('/bimbingan/{id}/update', 'DosenController@updateBimbingan');

        Route::get('/profile/{id}', 'DosenController@profile');
        Route::post('/profile/{id}/update', 'DosenController@update');
    });
});

Route::middleware(['auth'])->group(function () {

    Route::prefix('/koor-ta')->group(function () {

        //Dashboard
        Route::get('/dashboard', function () {
            return view('koor-ta.dashboard');
        });

        Route::get('/dashboard', 'KoorTAController@dashboard')->name('koor-ta.dashboard');

        Route::get('/admin', function () {
            return view('koor-ta.admin');
        });

        Route::get('/admin', 'KoorTAController@indexAdmin')->name('koor-ta.admin');
        Route::post('/admin/tambah/', 'KoorTAController@storeAdmin');
        Route::get('/admin/{id}/edit/', 'KoorTAController@editAdmin');
        Route::post('/admin/edit', 'KoorTAController@updateAdmin');
        Route::get('/admin/hapus/{id}', 'KoorTAController@destroyAdmin');

        Route::get('/pendaftaran', function () {
            return view('koor-ta.pendaftaran');
        });

        Route::get('/pendaftaran', 'KoorTAController@indexPendaftaran')->name('koor-ta.pendaftaran');

        Route::get('/bimbingan', function () {
            return view('koor-ta.bimbingan');
        });

        Route::get('/bimbingan', 'KoorTAController@indexBimbingan')->name('koor-ta.bimbingan');

        Route::get('/profile/{id}', 'KoorTAController@profile');
        Route::post('/profile/{id}/update', 'KoorTAController@updateProfile');
    });
});

Route::middleware(['auth'])->group(function () {

    Route::prefix('/mahasiswa')->group(function () {

        //Dashboard
        Route::get('/dashboard', function () {
            return view('mahasiswa.dashboard');
        })->name('mahasiswa.dashboard');

        Route::get('/pendaftaran', function () {
            return view('mahasiswa.pendaftaran');
        });

        Route::get('/pendaftaran/{id}', 'MahasiswaController@indexPendaftaran')->name('mahasiswa.pendaftaran');
        Route::post('/pendaftaran/{id}/tambah/', 'MahasiswaController@storePendaftaran');
        Route::get('/pendaftaran/{id}/edit/', 'MahasiswaController@editPendaftaran');
        Route::get('/pendaftaran/hapus/{id}', 'MahasiswaController@destroyPendaftaran');
        Route::post('/pendaftaran/{id}/uploadBerkas', 'MahasiswaController@uploadBerkas');
        Route::get('/pendaftaran/hapus/{id}', 'MahasiswaController@destroyBerkas');

        Route::get('/bimbingan', function () {
            return view('mahasiswa.bimbingan');
        });

        Route::get('/bimbingan/{id}', 'MahasiswaController@indexBimbingan')->name('mahasiswa.bimbingan');
        Route::post('/bimbingan/{id}/tambah/', 'MahasiswaController@storeBimbingan');
        Route::get('/bimbingan/{id}/edit/', 'MahasiswaController@editBimbingan');
        Route::post('/bimbingan/{id}/uploadBerkas', 'MahasiswaController@uploadBerkas');
        Route::get('/bimbingan/hapus/{id}', 'MahasiswaController@destroyBerkas');

        Route::get('/profile/{id}', 'MahasiswaController@profile');
        Route::post('/profile/{id}/update', 'MahasiswaController@updateProfile');
    });
});
