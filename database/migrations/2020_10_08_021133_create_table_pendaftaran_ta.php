<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePendaftaranTa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftaran_ta', function (Blueprint $table) {
            $table->increments('pendaftaran_id');
            $table->unsignedInteger('nim');
            $table->unsignedInteger('nidn');
            $table->string('pendaftaran_judul');
            $table->string('pendaftaran_status');
            $table->timestamps();

            $table->foreign('nim')->references('nim')->on('mahasiswa')
                ->onDelete('cascade');
            $table->foreign('nidn')->references('nidn')->on('dosen')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftaran_ta');
    }
}
