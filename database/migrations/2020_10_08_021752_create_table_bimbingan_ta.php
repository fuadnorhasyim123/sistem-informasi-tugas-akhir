<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBimbinganTa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bimbingan_ta', function (Blueprint $table) {
            $table->increments('bimbingan_id');
            $table->unsignedInteger('pendaftaran_id');
            $table->date('bimbingan_tgl');
            $table->string('bimbingan_ket');
            $table->timestamps();

            $table->foreign('pendaftaran_id')->references('pendaftaran_id')->on('pendaftaran_ta')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bimbingan_ta');
    }
}
